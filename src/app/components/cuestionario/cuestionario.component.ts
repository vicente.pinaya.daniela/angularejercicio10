import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-cuestionario',
  templateUrl: './cuestionario.component.html',
  styleUrls: ['./cuestionario.component.css']
})
export class CuestionarioComponent implements OnInit {

  cuestionario!: FormGroup;

  pregunta1: string ='1. A usted le gusta las películas de terror?';
  pregunta2: string = '2. Que color de bolígrafo usa?';
  pregunta3: string = '3. De que color es el caballo blanco de Napoleon?';
  pregunta4: string = '4. Que es más pesado, 1 kilo de plumas o 1 kilo de Oro?';
  pregunta5: string = '5. Cuantos dedos tiene una gallina en su pata?';

  constructor(private fb: FormBuilder) { 
    this.crearCuestionario()
  }

  ngOnInit(): void {
  }

  get aceptarNoValido(){
    return this.cuestionario.get('aceptar')?.invalid && this.cuestionario.get('aceptar')?.touched
  }

  crearCuestionario():void{
    console.log('crearCues');
    this.cuestionario = this.fb.group({
      resUno:[''],
      resDos: [''],
      resTres: [''],
      resCuatro: [''],
      resCinco: [''],
      aceptar:['']
    })
  }

  guardar(): void{
    console.log(this.cuestionario.value);
    console.log(`${this.pregunta1}\nR.-${this.cuestionario.value.resUno}`);
    console.log(`${this.pregunta2}\nR.-${this.cuestionario.value.resDos}`);
    console.log(`${this.pregunta3}\nR.-${this.cuestionario.value.resTres}`);
    console.log(`${this.pregunta4}\nR.-${this.cuestionario.value.resCuatro}`);
    console.log(`${this.pregunta5}\nR.-${this.cuestionario.value.resCinco}`);
    
  }

}
