import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Formulario42Component } from './formulario42.component';

describe('Formulario42Component', () => {
  let component: Formulario42Component;
  let fixture: ComponentFixture<Formulario42Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Formulario42Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Formulario42Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
